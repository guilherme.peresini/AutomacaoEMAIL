*** Settings ***
Resource                           ../Resource/Resource.robot
Suite SETUP                         Abrir navegador
Suite TEARDOWN                      Fechar navegador

*** Variable ***
${BOTAO_NOVA_CAMPANHA}             id=btInicio
${GER_NOVA_CAMPANHA}               xpath=//input[@value='Nova Campanha']
${BOTAO_PAUSAR}                    id=btPausa
${BOTAO_REINICIAR}                 id=btReiniciar
${BOTAO_CANCELAR}                  id=btCancel

#Variáveis de cliente, tipo de template, layout de arquivos e arquivo
${CLIENTE}                         Empresa teste
${TEMPLATE}                        Template QA2
${LAYOUT}                          TesteQA
${ARQUIVO_SIMPLES}                 ${CURDIR}//arquivos//email simples.txt
${ARQUIVO_INVALIDO}                ${CURDIR}//arquivos//email invalido.txt
${ARQUIVO_EMAIL_REPETIDO}          ${CURDIR}//arquivos//email repetido.txt
${ARQUIVO_BLACKLIST}               ${CURDIR}//arquivos//email blacklist.txt
${ARQUIVO_500K}                    ${CURDIR}//arquivos//arquivo 500k.txt
${ARQUIVO_1K}                      ${CURDIR}//arquivos//arquivo 1M.txt

#Locators do Passo 1
${COMBO_CLIENTE}                   id=idcli
${RADIO_TEMPLATE}                  opcao_template
${COMBO_TEMPLATE}                  id=template
${INPUT_TITULO}                    id=titulo
${BOTAO_PASSO_2}                   xpath=//form[@id='frmImp']//input[@type='submit' and @value='Passo 2 - Importação de Arquivos']
${VOLTAR}                          xpath=//input[@value='Voltar']

#Locators do Passo 2
${COMBO_LAYOUT}                    id=idlayout
${SELECT FILE}                     id=files
${BOTAO_PASSO_3_PERIODOS}          id=clickSubmit
${BOTAO_LIMPAR}                    id=clearAll
${BOTAO_PASSO_3_CONFIRMAR}         id=sbmPasso3
${COMBO_LOTES}                     id=lotes
${INPUT_QTD_REGISTROS}             id=qtlot_1
${COMBO_AGENDAMENTO_1}             xpath=//*[@id='mgridlote']//tr/td/strong[contains(text(),'1')]/../..//*[@id='data']
${INPUT_HORA}                      id=hragend_1
${INPUT_HORA_2}                    id=hragend_2
${COMBO_AGENDAMENTO_2}             xpath=//*[@id='mgridlote']//tr/td/strong[contains(text(),'2')]/../..//*[@id='data']

#Locators do Passo 3
${BOTAO_CONFIRMAR_ENVIO}           xpath=//form[@name='frmLote']//input[@value='Confirmar Envio']

#Locartos Cancelamento de Campanha
${CHECKBOX}                        xpath=//*[@id='mgridlote']//tbody/tr[1]//input[@id='checkidcamp']
${BOTAO_CONFIRMAR_CANCELAMENTO}    xpath=//*[@value='Confirmar Cancelamento']

*** Test Case ***
Validação de criação campanha de email
    [tags]    Daily    Regressao
    Dado que estou no painel de email
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando preencher o formulário do Passo 2 corretamente com "arquivo simples"
    Então deve ser exibida a página "Importar Job - Passo 3"

    Quando clicar em "Confirmar Envio"
    Então a campanha criada deve ser exibida na página principal

Validação de Pausar campanha de email
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando preencher o formulário do Passo 2 corretamente com "arquivo simples"
    Então deve ser exibida a página "Importar Job - Passo 3"

    Quando clicar em "Confirmar Envio"
    Então a campanha criada deve ser exibida na página principal

    Quando marcar a campanha e clicar em "Pausar"
    Então a campanha deve ser exibida como "Pausa"

Validação de Reiniciar campanha de email
    [tags]    Regressao
    Navegar até a dashboard
    Quando marcar a campanha e clicar em "Reiniciar"
    Então a campanha deve ser exibida como "A Enviar"

Validação de Cancelar campanha de email
    [tags]    Regressao
    Navegar até a dashboard
    Quando marcar a campanha e clicar em "Cancelar"
    Então deve ser exibida a página "Cancelar Campanha"

    Quando selecionar o checkbox e clicar em "Confirmar Cancelamento"
    Então deve ser exibido o alert "Deseja cancelar os Lotes selecionados?"
    Então a campanha não deve ser exibida

Validação de lotes com mais registros
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando selecionar um arquivo simples e clicar em "Passo 3 - Períodos de Envio"
    Então deve ser exibida a página "Escalonamento"

    Quando informar uma quantidade "maior" de registros
    Então deve ser exibido mensagem de validação "O total informado não está correto"

Validação de lotes com menos registros
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando selecionar um arquivo simples e clicar em "Passo 3 - Períodos de Envio"
    Então deve ser exibida a página "Escalonamento"

    Quando informar uma quantidade "menor" de registros
    Então deve ser exibido mensagem de validação "Voce deve informar uma quantidade válida"

Validação de agendamento de lotes sem informar hora
    [tags]    Regressao                           
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando selecionar um arquivo simples e clicar em "Passo 3 - Períodos de Envio"
    Então deve ser exibida a página "Escalonamento"

    Quando agendar um lote sem informar a hora de envio
    Então deve ser exibido mensagem de validação "Preencha corretamente os campos de lote!"

Validação de agendamento de lotes
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando selecionar um arquivo simples e clicar em "Passo 3 - Períodos de Envio"
    Então deve ser exibida a página "Escalonamento"

    Quando agendar um lote e clicar em "Passo 3 - Confirmação de Envio"
    Então a data agendada deve ser exibida na tela

Validação de campanhas com 2 lotes
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando selecionar um arquivo simples e clicar em "Passo 3 - Períodos de Envio"
    Então deve ser exibida a página "Escalonamento"

    Quando agendar 2 lotes e clicar em "Passo 3 - Confirmação de Envio"
    Então as datas agendadas devem ser exibidas na tela

    Quando clicar em "Confirmar Envio"
    Então a campanha criada deve ser exibida na página principal

    Quando clicar em "Detalhes do JOB"
    Então deve ser exibido a quantidade de lotes da campanha

# Validação de criação campanha de email com 500K registros
#                                  Navegar até a dashboard
#                                  Quando clicar em "Nova campanha"
#                                  Então deve ser exibida a página "Importar Job - Passo 1"

#                                  Quando preencher o formulário do Passo 1 corretamente
#                                  Então deve ser exibida a página "Importar Job - Passo 2"

#                                  Quando preencher o formulário do Passo 2 corretamente com "arquivo 500K"
#                                  Então deve ser exibida a página "Importar Job - Passo 3"

#                                  Quando clicar em "Confirmar Envio"
#                                  Então a campanha criar deve ser exibida na página principal

# Validação de criação campanha de email com 1M de registros
#                                  Navegar até a dashboard
#                                  Quando clicar em "Nova campanha"
#                                  Então deve ser exibida a página "Importar Job - Passo 1"

#                                  Quando preencher o formulário do Passo 1 corretamente
#                                  Então deve ser exibida a página "Importar Job - Passo 2"

#                                  Quando preencher o formulário do Passo 2 corretamente com "arquivo 1M"
#                                  Então deve ser exibida a página "Importar Job - Passo 3"

#                                  Quando clicar em "Confirmar Envio"
#                                  Então a campanha criar deve ser exibida na página principal

Validação de email inválido no arquivo
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando selecionar um arquivo que contém "email inválido"
    Então deve ser exibida a contagem de emails "inválidos" no arquivo

Validação de email repetido no arquivo
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando selecionar um arquivo que contém "email repetido"
    Então deve ser exibida a contagem de emails "repetidos" no arquivo

Validação de email de blacklist no arquivo
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando selecionar um arquivo que contém "email blacklist"
    Então deve ser exibida a contagem de emails "blacklist" no arquivo

Validação de acesso à importação de campanha pelo Jobs/Importar
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Importar"
    Então deve ser exibida a página "Importar Job - Passo 1"

Validação de acesso à importação de campanha pelo Jobs/Gerenciar/Nova Campanha
    [tags]    Regressao
    Navegar até a dashboard
    Quando ir em "Jobs" e clicar em "Gerenciar"
    Então deve ser exibida a página "Campanhas"

    Quando clicar em "Nova Campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

Validação da opção voltar no passo 1 de campanhas
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando clicar em "Voltar"
    Então deve ser exibido o dashboard de campanhas

Validação da opção voltar no passo 2 de campanhas
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando clicar em "Voltar"
    Então deve ser exibida a página "Importar Job - Passo 1"

Validação da opção voltar no passo 3 de campanhas
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando preencher o formulário do Passo 2 corretamente com "arquivo simples"
    Então deve ser exibida a página "Importar Job - Passo 3"

    Quando clicar em "Voltar"
    Então deve ser exibida a página "Importar Job - Passo 1"

Validação da opção limpar arquivos no passo 2 de campanhas
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando preencher o formulário do Passo 2 e clicar em Limpar Arquivos
    Então o arquivo inserido deve ser removido da página

Validação de submissão do passo 1 sem preencher o formulário
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando submeter o formulário do passo 1 sem preencher os dados
    Então deve ser exibido o alert "Selecione um cliente"

Validação de submissão do passo 1 sem preencher o campo Cliente
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando submeter o formulário do passo 1 sem preencher o campo "Cliente"
    Então deve ser exibido o alert "Selecione um cliente"

Validação de submissão do passo 1 sem preencher o campo Template
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando submeter o formulário do passo 1 sem preencher o campo "Template"
    Então deve ser exibido o alert "Insira um título"

Validação de submissão do passo 1 sem preencher o campo Título
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando submeter o formulário do passo 1 sem preencher o campo "Título do Email"
    Então deve ser exibido o alert "Insira um título"

Validação de submissão do passo 2 sem preencher o formulário
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando submeter o formulário do passo 2 sem preencher os dados
    Então deve ser exibido o alert "Selecione os arquivos."

Validação de submissão do passo 2 sem preencher o campo layout
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando submeter o formulário do passo 2 sem preencher o campo "Layout"
    Então deve ser exibido o alert "Os arquivos inválidos não serão processados!"

Validação de submissão do passo 2 sem selecionar um arquivo
    [tags]    Regressao
    Navegar até a dashboard
    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando submeter o formulário do passo 2 sem selecionar um arquivo
    Então deve ser exibido o alert "Selecione os arquivos."

Validação de criação de campanha com login externo
    [tags]    Daily    Regressao
    Realizar logout

    Quando preencher informações de login externo
    Então deve ser exibido o dashboard de campanhas

    Quando clicar em "Nova campanha"
    Então deve ser exibida a página "Importar Job - Passo 1"

    Quando preencher o formulário do Passo 1 corretamente
    Então deve ser exibida a página "Importar Job - Passo 2"

    Quando preencher o formulário do Passo 2 corretamente com "arquivo simples"
    Então deve ser exibida a página "Importar Job - Passo 3"

    Quando clicar em "Confirmar Envio"
    Então a campanha criada deve ser exibida na página principal

*** Keywords ***
Dado que estou no painel de email
    Acessar painel

Quando ir em "${ABA}" e clicar em "${OPCAO}"
    Acessar menus       ${ABA}      ${OPCAO}

Quando clicar em "${ELEMENTO}"
    Run Keyword If  '${ELEMENTO}'=='Nova campanha'        Click Element     ${BOTAO_NOVA_CAMPANHA}
    ...    ELSE IF  '${ELEMENTO}'=='Voltar'               Click Element     ${VOLTAR}
    ...    ELSE IF  '${ELEMENTO}'=='Confirmar Envio'      Run Keywords      Click Element                                                                       ${BOTAO_CONFIRMAR_ENVIO}    AND     Wait Until Page Contains    Campanhas em andamento   ${TIMEOUT}
    ...    ELSE IF  '${ELEMENTO}'=='Nova Campanha'        Click Element     ${GER_NOVA_CAMPANHA}
    ...    ELSE IF  '${ELEMENTO}'=='Detalhes do JOB'      Click Element     xpath=//table[@id='mgrid']//td/span[contains(text(),'${ID_CAMPANHA}')]/../..//a

Quando preencher informações de login externo
    Input Text          ${CAMPO_EMAIL}      ${LOGIN.externo}
    Input Text          ${CAMPO_SENHA}      ${LOGIN.senha}
    Click Element       ${BOTAO_ENTRAR}

Quando preencher o formulário do Passo 1 corretamente
    Sleep                       2s
    Select From List By Label   ${COMBO_CLIENTE}    ${CLIENTE}
    Select Radio Button         ${RADIO_TEMPLATE}   N
    Sleep                       2s
    Select From List By Label   ${COMBO_TEMPLATE}   ${TEMPLATE}
    Sleep                       2s
    Click Element               ${BOTAO_PASSO_2}

Quando marcar a campanha e clicar em "${ACAO}"
    Select Checkbox      xpath=//table[@id='mgrid']//td/span[contains(text(),'${ID_CAMPANHA}')]/../../td[1]/input[@id='checkidcamp']
    Run Keyword If      '${ACAO}'=='Pausar'         Run Keywords    Click Element   ${BOTAO_PAUSAR}     AND     Alert Should Be Present     text=Deseja pausar a Campanha selecionada?
    ...    ELSE IF      '${ACAO}'=='Reiniciar'      Run Keywords    Click Element   ${BOTAO_REINICIAR}  AND     Alert Should Be Present     text=Deseja reiniciar a Campanha selecionada?
    ...    ELSE IF      '${ACAO}'=='Cancelar'       Run Keywords    Click Element   ${BOTAO_CANCELAR}   AND     Alert Should Be Present     text=Deseja cancelar a Campanha selecionada?

Quando submeter o formulário do passo 1 sem preencher os dados
    Click Element   ${BOTAO_PASSO_2}

Quando submeter o formulário do passo 1 sem preencher o campo "Cliente"
    Select Radio Button         ${RADIO_TEMPLATE}   N
    sleep                       2s
    Select From List By Label   ${COMBO_TEMPLATE}   ${TEMPLATE}
    Click Element               ${BOTAO_PASSO_2}

Quando submeter o formulário do passo 1 sem preencher o campo "Template"
    Select From List By Label   ${COMBO_CLIENTE}    ${CLIENTE}
    Select Radio Button         ${RADIO_TEMPLATE}   N
    Sleep                       2s
    Click Element               ${BOTAO_PASSO_2}

Quando submeter o formulário do passo 1 sem preencher o campo "Título do Email"
    Select From List By Label   ${COMBO_CLIENTE}    ${CLIENTE}
    Sleep                       2s
    Click Element               ${BOTAO_PASSO_2}

Quando preencher o formulário do Passo 2 corretamente com "${TIPO_ARQUIVO}"
    Sleep                               2s
    Select From List By Label           ${COMBO_LAYOUT}                          ${LAYOUT}
    ${LINHAS}=                          Run Keyword If                          '${TIPO_ARQUIVO}'=='arquivo simples'    Contar linhas do arquivo    ${ARQUIVO_SIMPLES}
    ...    ELSE IF                      '${TIPO_ARQUIVO}'=='arquivo 500K'       Contar linhas do arquivo                ${ARQUIVO_500K}
    ...    ELSE IF                      '${TIPO_ARQUIVO}'=='arquivo 1M'         Contar linhas do arquivo                ${ARQUIVO_1K}
    Run Keyword If                      '${TIPO_ARQUIVO}'=='arquivo simples'    Run Keywords                            Choose File                 ${SELECT FILE}      ${ARQUIVO_SIMPLES}      AND     Wait Until Element Is Visible   xpath=//*[@id='frmImp']//strong[contains(text(),'Done')]    ${TIMEOUT}
    ...    ELSE IF                      '${TIPO_ARQUIVO}'=='arquivo 500K'       Run Keywords                            Choose File                 ${SELECT FILE}      ${ARQUIVO_500K}         AND     Wait Until Element Is Visible   xpath=//*[@id='frmImp']//strong[contains(text(),'Done')]    600s
    ...    ELSE IF                      '${TIPO_ARQUIVO}'=='arquivo 1M'         Run Keywords                            Choose File                 ${SELECT FILE}      ${ARQUIVO_1K}           AND     Wait Until Element Is Visible   xpath=//*[@id='frmImp']//strong[contains(text(),'Done')]    900s
    Click Element                       ${BOTAO_PASSO_3_PERIODOS}
    Wait Until Page Contains Element    xpath=//*[@class='menu-row-titulo']/p[contains(text(),'Escalonamento')]   ${TIMEOUT}
    Click Element                       ${BOTAO_PASSO_3_CONFIRMAR}
    Wait Until Page Contains            Importar Job - Passo 3    ${TIMEOUT}
    ${ID_CAMPANHA}                      Get Value                               xpath=//form[@name='frmLote']/input[@name='str_camp']
    Set Global Variable                 ${ID_CAMPANHA}

Quando selecionar um arquivo simples e clicar em "Passo 3 - Períodos de Envio"
    Select From List By Label       ${COMBO_LAYOUT}     ${LAYOUT}
    Choose File                     ${SELECT FILE}      ${ARQUIVO_SIMPLES}
    Wait Until Element Is Visible   xpath=//*[@id='frmImp']//strong[contains(text(),'Done')]    ${TIMEOUT}
    Click Element                   ${BOTAO_PASSO_3_PERIODOS}

Quando submeter o formulário do passo 2 sem preencher os dados
    Click Element   ${BOTAO_PASSO_3_PERIODOS}

Quando submeter o formulário do passo 2 sem preencher o campo "Layout"
    Choose File                     ${SELECT FILE}      ${ARQUIVO_SIMPLES}
    Wait Until Element Is Visible   xpath=//*[@id='frmImp']//strong[contains(text(),'Done')]    ${TIMEOUT}
    Click Element                   ${BOTAO_PASSO_3_PERIODOS}

Quando submeter o formulário do passo 2 sem selecionar um arquivo
    Select From List By Label   ${COMBO_LAYOUT}             ${LAYOUT}
    Click Element               ${BOTAO_PASSO_3_PERIODOS}

Quando preencher o formulário do Passo 2 e clicar em Limpar Arquivos
    Select From List By Label       ${COMBO_LAYOUT}     ${LAYOUT}
    Choose File                     ${SELECT FILE}      ${ARQUIVO_SIMPLES}
    Wait Until Element Is Visible   xpath=//*[@id='frmImp']//strong[contains(text(),'Done')]   ${TIMEOUT}
    Click Element                   ${BOTAO_LIMPAR}

Quando selecionar um arquivo que contém "${TIPO_ARQUIVO}"
    Select From List By Label           ${COMBO_LAYOUT}                         ${LAYOUT}
    ${LINHAS}=                          Run Keyword If                          '${TIPO_ARQUIVO}'=='email inválido'     Contar linhas do arquivo    ${ARQUIVO_INVALIDO}
    ...    ELSE IF                      '${TIPO_ARQUIVO}'=='email repetido'     Contar linhas do arquivo    ${ARQUIVO_EMAIL_REPETIDO}
    ...    ELSE IF                      '${TIPO_ARQUIVO}'=='email blacklist'    Contar linhas do arquivo    ${ARQUIVO_BLACKLIST}
    Run Keyword If                      '${TIPO_ARQUIVO}'=='email inválido'     Choose File                 ${SELECT FILE}                          ${ARQUIVO_INVALIDO}
    ...    ELSE IF                      '${TIPO_ARQUIVO}'=='email repetido'     Choose File                 ${SELECT FILE}                          ${ARQUIVO_EMAIL_REPETIDO}
    ...    ELSE IF                      '${TIPO_ARQUIVO}'=='email blacklist'    Choose File                 ${SELECT FILE}                          ${ARQUIVO_BLACKLIST}
    Wait Until Element Is Visible       xpath=//*[@id='frmImp']//strong[contains(text(),'Done')]     ${TIMEOUT}
    Click Element                       ${BOTAO_PASSO_3_PERIODOS}
    Wait Until Page Contains Element    xpath=//*[@class='menu-row-titulo']/p[contains(text(),'Escalonamento')]    ${TIMEOUT}
    Page Should Contain Element         xpath=//*[@id='mgrid']/tbody//td[3][contains(text(), '${LINHAS}')]

Quando informar uma quantidade "${QTD}" de registros
    Run Keyword If  '${QTD}'=='maior'   Run Keywords    Input Text  ${INPUT_QTD_REGISTROS}  10  AND     Click Element   ${BOTAO_PASSO_3_CONFIRMAR}
    ...    ELSE IF  '${QTD}'=='menor'   Run Keywords    Input Text  ${INPUT_QTD_REGISTROS}  0   AND     Click Element   ${BOTAO_PASSO_3_CONFIRMAR}

Quando agendar um lote e clicar em "Passo 3 - Confirmação de Envio"
    ${DATA}=                    Acrescentar dias à data atual   2 days
    Set Global Variable         ${DATA}
    Select From List By Label   ${COMBO_AGENDAMENTO_1}          ${DATA}
    Input Text                  ${INPUT_HORA}                   12:00
    Click Element               ${BOTAO_PASSO_3_CONFIRMAR}

Quando agendar 2 lotes e clicar em "Passo 3 - Confirmação de Envio"
    Select From List By Label   ${COMBO_LOTES}                  2
    ${DATA}=                    Acrescentar dias à data atual   2 days
    Set Global Variable         ${DATA}
    Select From List By Label   ${COMBO_AGENDAMENTO_2}          ${DATA}
    Input Text                  ${INPUT_HORA_2}                 12:00
    Click Element               ${BOTAO_PASSO_3_CONFIRMAR}

Quando agendar um lote sem informar a hora de envio
    ${DATA}=                    Acrescentar dias à data atual   2 days
    Set Global Variable         ${DATA}
    Select From List By Label   ${COMBO_AGENDAMENTO_1}          ${DATA}
    Click Element               ${BOTAO_PASSO_3_CONFIRMAR}

Quando selecionar o checkbox e clicar em "Confirmar Cancelamento"
    Click Element   ${CHECKBOX}
    Click Element   ${BOTAO_CONFIRMAR_CANCELAMENTO}

Então deve ser exibida a página "${PAGINA}"
    Run Keyword If  '${PAGINA}'=='Escalonamento'    Page Should Contain Element     xpath=//*[@class='menu-row-titulo']/p[contains(text(),'Escalonamento')]
    ...       ELSE  Run Keywords    Sleep   1s  AND  Page Should Contain Element    xpath=//h5[contains(text(),'${PAGINA}')]

Então a campanha criada deve ser exibida na página principal
    Page Should Contain Element     xpath=//table[@id='mgrid']//td/span[contains(text(),'${ID_CAMPANHA}')]
    
Então deve ser exibido o dashboard de campanhas
    Wait Until Page Contains    Campanhas em andamento      ${TIMEOUT}
    Page Should Contain         Campanhas em andamento

Então o arquivo inserido deve ser removido da página
    Element Should Not Be Visible   xpath=//*[@id='frmImp']//strong[contains(text(),'Done')]

Então deve ser exibido o alert "${TEXTO_ALERT}"
    Alert Should Be Present     ${TEXTO_ALERT}

Então deve ser exibida a contagem de emails "${VALIDACAO}" no arquivo
    Run Keyword If  '${VALIDACAO}'=='inválidos'     Page Should Contain Element     xpath=//*[@id='mgrid']/tbody//td[4][contains(text(), '1')]
    ...    ELSE IF  '${VALIDACAO}'=='repetidos'     Page Should Contain Element     xpath=//*[@id='mgrid']/tbody//td[6][contains(text(), '1')]
    ...    ELSE IF  '${VALIDACAO}'=='blacklist'     Page Should Contain Element     xpath=//*[@id='mgrid']/tbody//td[5][contains(text(), '1')]

Então a campanha deve ser exibida como "${STATUS}"
    Page Should Contain Element     xpath=//table[@id='mgrid']//td/span[contains(text(),'${ID_CAMPANHA}')]
    Run Keyword If                  '${STATUS}'=='Pausa'        Wait Until Page Contains Element    xpath=//table[@id='mgrid']//td/span[contains(text(),'${ID_CAMPANHA}')]/../../td[4]/span[contains(text(), 'Pausa')]      ${TIMEOUT}
    ...    ELSE IF                  '${STATUS}'=='A Enviar'     Wait Until Page Contains Element    xpath=//table[@id='mgrid']//td/span[contains(text(),'${ID_CAMPANHA}')]/../../td[4]/span[contains(text(), 'A Enviar')]   ${TIMEOUT}

Então a campanha não deve ser exibida
    Page Should Not Contain Element     xpath=//table[@id='mgrid']//td/span[contains(text(),'${ID_CAMPANHA}')]

Então deve ser exibido mensagem de validação "${MSG}"
    Element Should Be Visible       xpath=//div[@id='validateBox'][contains(text(),'${MSG}')]

Então a data agendada deve ser exibida na tela
    Wait Until Page Contains    ${DATA} - 12:00    ${TIMEOUT}

Então as datas agendadas devem ser exibidas na tela
    Wait Until Page Contains Element    xpath=//h5[contains(text(),'Importar Job - Passo 3')]     ${TIMEOUT}
    Page Should Contain Element         xpath=//strong[contains(text(),'Lote 1')]
    Page Should Contain Element         xpath=//strong[contains(text(),'Envio Imediato!')]
    Page Should Contain Element         xpath=//strong[contains(text(),'Lote 2')]
    Page Should Contain Element         xpath=//strong[contains(text(),'Agendado para: ')]
    Page Should Contain                 ${DATA} - 12:00
    ${ID_CAMPANHA}                      Get Value           xpath=//form[@name='frmLote']/input[@name='str_camp']
    Set Global Variable                 ${ID_CAMPANHA}

Então deve ser exibido a quantidade de lotes da campanha
    Page Should Contain     Lotes de Envio: 2