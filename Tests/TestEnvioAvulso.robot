*** Settings ***
Resource               ../Resource/Resource.robot
Suite SETUP             Abrir navegador
Suite TEARDOWN          Fechar navegador

*** Variables ***
#Locators frm-envio-rapido.php
${SELECT_CLIENTE}      id=idcli
${SELECT_TEMPLATE}     id=template
${INPUT_EMAIL}         id=emails
${INPUT_TITULO}        id=titulo
${BOTAO_VOLTAR}        xpath=//input[@value='Voltar']
${BOTAO_ENVIAR}        xpath=//input[@value='Enviar']

${CLIENTE}             Empresa teste
${TEMPLATE}            Template QA2
${EMAIL}               guilherme.peresini@pgmais.com.br
${EMAIL_BLACKLIST}     hcristina275@gmail.com

#Locators AvulsoApi
${SELECT_CARTEIRA}     id=carteira
${INPUT_DT_INICIAL}    id=dtini
${INPUT_DT_FINAL}      id=dtfim
${BOTAO_BUSCAR}        id=sh_log

*** Test Cases ***
Validação de envio avulso
    [Tags]    Daily    Regressao
    Dado que estou na tela de envio rápido
    Quando preencher todo o formulário e clicar em Enviar
    Então deve ser exibiba a mensagem "Email enviado com sucesso!"

Validação de envio para email em blacklist
    [Tags]    Regressao
    Navegar até a dashboard
    Acessar menu Jobs - Envio rápido
    Quando preencher o formulário com email em blacklist e clicar em Enviar
    Então deve ser exibiba a mensagem "Não foi possível enviar, email em blacklist. Razão: Adicionado pelo painel"

Validação de envio sem preencher os campos
    [Tags]    Regressao
    Navegar até a dashboard
    Acessar menu Jobs - Envio rápido
    Quando clicar em Enviar com o formulário em branco
    Então deve ser exibibas mensagens de validação

Validação de envio sem preencher o campo cliente
    [Tags]    Regressao
    Navegar até a dashboard
    Acessar menu Jobs - Envio rápido
    Quando clicar em enviar sem preencher "cliente"
    Então deve ser exibida a mensagem de validação para o campo "cliente"

Validação de envio sem preencher o campo template
    [Tags]    Regressao
    Navegar até a dashboard
    Acessar menu Jobs - Envio rápido
    Quando clicar em enviar sem preencher "template"
    Então deve ser exibida a mensagem de validação para o campo "template"

Validação de envio sem preencher o campo emails destino
    [Tags]    Regressao
    Navegar até a dashboard
    Acessar menu Jobs - Envio rápido
    Quando clicar em enviar sem preencher "email"
    Então deve ser exibida a mensagem de validação para o campo "email"

Validação de envio sem preencher o campo título do email
    [Tags]    Regressao
    Navegar até a dashboard
    Acessar menu Jobs - Envio rápido
    Quando clicar em enviar sem preencher "título"
    Então deve ser exibida a mensagem de validação para o campo "título"

Validação de envio com mensagem em branco
    [Tags]    Regressao
    Navegar até a dashboard
    Acessar menu Jobs - Envio rápido
    Quando clicar em enviar sem preencher "mensagem"
    Então deve ser exibiba a mensagem "Mensagem em branco"

*** Keywords ***
Dado que estou na tela de envio rápido
    Acessar painel
    Acessar menus       Jobs    Envio Rapido

Quando clicar em enviar sem preencher "${CAMPO}"
    Run Keyword If  '${CAMPO}'=='cliente'        Run Keywords       Sleep       2s      AND     Select From List By Label     ${SELECT_TEMPLATE}    ${TEMPLATE}     AND     Input Text      ${INPUT_EMAIL}      ${EMAIL}    AND                          Click Element         ${BOTAO_ENVIAR}
    ...    ELSE IF  '${CAMPO}'=='template'       Run Keywords       Sleep       2s      AND     Select From List By Label     ${SELECT_CLIENTE}     ${CLIENTE}      AND     Input Text      ${INPUT_EMAIL}      ${EMAIL}    AND                          Click Element         ${BOTAO_ENVIAR}
    ...    ELSE IF  '${CAMPO}'=='email'          Run Keywords       Sleep       2s      AND     Select From List By Label     ${SELECT_CLIENTE}     ${CLIENTE}      AND     Sleep           2s                  AND         Select From List By Label    ${SELECT_TEMPLATE}    ${TEMPLATE}          AND     Click Element    ${BOTAO_ENVIAR}
    ...    ELSE IF  '${CAMPO}'=='título'         Run Keywords       Sleep       2s      AND     Select From List By Label     ${SELECT_CLIENTE}     ${CLIENTE}      AND     Sleep           2s                  AND         Select From List By Label    ${SELECT_TEMPLATE}    ${TEMPLATE}          AND     Input Text       ${INPUT_EMAIL}     ${EMAIL}    AND     Clear Element Text    ${INPUT_TITULO}    AND    Click Element         ${BOTAO_ENVIAR}
    ...    ELSE IF  '${CAMPO}'=='mensagem'       Run Keywords       Sleep       2s      AND     Select From List By Label     ${SELECT_CLIENTE}     ${CLIENTE}      AND     Sleep           2s                  AND         Select From List By Label    ${SELECT_TEMPLATE}    ${TEMPLATE}          AND     Input Text       ${INPUT_EMAIL}     ${EMAIL}    AND     Select Frame          id=elm1_ifr        AND    Clear Element Text    xpath=//*[@id='tinymce']    AND    Unselect Frame    AND    Click Element    ${BOTAO_ENVIAR}

Quando preencher todo o formulário e clicar em Enviar
    Sleep                           2s   
    Select From List By Label       ${SELECT_CLIENTE}       ${CLIENTE}
    Sleep                           2s 
    Select From List By Label       ${SELECT_TEMPLATE}      ${TEMPLATE}
    Sleep                           2s
    Input Text                      ${INPUT_EMAIL}          ${EMAIL}
    Sleep                           2s
    Click Element                   ${BOTAO_ENVIAR}
    ${DATA}=                        Obter data atual 
    Set Global Variable             ${DATA}

Quando preencher o formulário com email em blacklist e clicar em Enviar
    Sleep                           2s
    Select From List By Label       ${SELECT_CLIENTE}       ${CLIENTE}
    Sleep                           2s
    Select From List By Label       ${SELECT_TEMPLATE}      ${TEMPLATE}
    Input Text                      ${INPUT_EMAIL}          ${EMAIL_BLACKLIST}
    Click Element                   ${BOTAO_ENVIAR}

Quando realizar o filtro e clicar em Buscar
    Select From List By Label           ${SELECT_CARTEIRA}                                  ${CLIENTE}
    ${DT_FILTRO}=                       Convert Date                                        ${DATA}                             result_format=%d/%m/%Y
    Input Text                          ${INPUT_DT_INICIAL}                                 ${DT_FILTRO}
    Input Text                          ${INPUT_DT_FINAL}                                   ${DT_FILTRO}
    Input Text                          id=pesquisa_email                                   guilherme.peresini@pgmais.com.br
    Click Element                       ${BOTAO_BUSCAR}
    wait Until Page Contains Element    xpath=//*[@id="dynatable-per-page-tabela-avulso"]   ${TIMEOUT}
    Select From List By Label           xpath=//*[@id="dynatable-per-page-tabela-avulso"]   100

Quando acessar Gerenciar AvulsoApi
    Acessar menus       Jobs        Gerenciar Avulso/API

Quando clicar em Enviar com o formulário em branco
    Click Element       ${BOTAO_ENVIAR}

Acessar menu Jobs - Envio rápido
    Acessar menus       Jobs        Envio Rapido

Então deve ser exibiba a mensagem "${MSG}"
    Element Should Be Visible       xpath=//div[contains(text(),'${MSG}')]

Então deve ser exibida a página "${PAGINA}"
    Page Should Contain Element     xpath=//h5[contains(text(),'${PAGINA}')]

Então deve ser exibida a mensagem de validação para o campo "${CAMPO}"
    Run Keyword If  '${CAMPO}'=='cliente'   Element Should Be Visible    id=idErroCliente
    ...    ELSE IF  '${CAMPO}'=='template'  Element Should Be Visible    id=idErroTemplate
    ...    ELSE IF  '${CAMPO}'=='email'     Element Should Be Visible    id=idErroEmail
    ...    ELSE IF  '${CAMPO}'=='título'    Element Should Be Visible    id=idErroTitulo

Então deve ser exibibas mensagens de validação
    Element Should Be Visible   id=idErroCliente
    Element Should Be Visible   id=idErroTemplate
    Element Should Be Visible   id=idErroEmail
    Element Should Be Visible   id=idErroTitulo

Então deve ser listado o email enviado
    ${DT_TABLE}=                    Convert Date                                                                                             ${DATA}    result_format=%d/%m/%Y %H:
    Wait Until Element Is Visible   xpath=//table[@id='tabela-avulso']//tbody/tr[position()=last()]/td[4][contains(text(),'${DT_TABLE}')]   ${TIMEOUT}
    Page Should Contain Element     xpath=//table[@id='tabela-avulso']//tbody/tr[position()=last()]/td[4][contains(text(),'${DT_TABLE}')]