*** Settings ***
Library               SeleniumLibrary
Library               String
Library               OperatingSystem
Library               FakerLibrary
Library               DateTime
Library               Collections

*** Variables ***
${BROWSER}            chrome
${URL_EMAIL}          https://email.maisresultado.com.br/
${URL_PAINEL}         https://painel.maisresultado.com.br/
${TIMEOUT}            120s

${CAMPO_EMAIL}        id=username
${CAMPO_SENHA}        id=password
${BOTAO_ENTRAR}       xpath=//*[@id='login_form']//button
${COMBO_EMPRESAS}     id=idcbemp
${BOTAO_CONFIRMAR}    xpath=//*[@id='login_company']//button[contains(text(), 'Confirmar')]

&{LOGIN}              username=teste@email.com                                                 externo=guilherme.peresini@pgmais.com.br    senha=Pgmais123    empresa=QA Pg Mais

*** Keywords ***
Abrir navegador
    Open Browser                   about:blank    ${BROWSER}  options=add_experimental_option('excludeSwitches', ['enable-logging'])
    Maximize Browser Window  

Fechar navegador
    Close Browser

Realizar login
    [Arguments]                         ${LOGIN.username}   ${LOGIN.senha}
    Go To                               ${URL_EMAIL}
    Input Text                          ${CAMPO_EMAIL}      ${LOGIN.username}
    Input Text                          ${CAMPO_SENHA}      ${LOGIN.senha}
    Click Element                       ${BOTAO_ENTRAR}
    Wait Until Element Is Visible       id=login_company    10s

Navegar até a dashboard
    Go To   ${URL_EMAIL}

Realizar login com usuario externo
    [Arguments]     ${LOGIN.externo}    ${LOGIN.senha}
    Go To           ${URL_EMAIL}
    Input Text      ${CAMPO_EMAIL}      ${LOGIN.username}
    Input Text      ${CAMPO_SENHA}      ${LOGIN.senha}
    Click Element   ${BOTAO_ENTRAR}

Selecionar empresa
    Select From List By Label           ${COMBO_EMPRESAS}       ${LOGIN.empresa}
    Click Element                       ${BOTAO_CONFIRMAR}
    Wait Until Page Contains Element    id=icone_usuario        10s

Selecionar empresa painel geral
    Select From List By Label           id=empresa              ${LOGIN.empresa}
    Click Element                       ${BOTAO_CONFIRMAR}
    Wait Until Page Contains Element    id=icone_usuario        10s

Acessar painel
    Realizar login      ${LOGIN.username}       ${LOGIN.senha}
    Selecionar empresa

Realizar logout
    Click Element                   id=icone_usuario
    Wait Until Element Is Visible   xpath=//*[@id='menu_pessoal']
    Click Element                   xpath=//a[contains(text(), 'Sair')]
    Page Should Contain             Bem vindo ao painel Email!


Contar linhas do arquivo
    [Arguments]             ${ARQUIVO}
    ${CONTEUDO_ARQUIVO}     Get File            ${ARQUIVO}
    ${LINHAS}               Get Line Count      ${CONTEUDO_ARQUIVO}
    [Return]                ${LINHAS}

Acessar menus
    [Arguments]             ${ABA}      ${OPCAO}
    Mouse Down On Link      xpath=//div[@id='menu']//a[contains(text(), '${ABA}')]
    Click Element           xpath=//div[@class='anylinkmenu']//a[contains(text(),'${OPCAO}')]

Gerar nome randomico
    ${CLIENTE}=     Name
    [Return]        ${CLIENTE}

Obter data atual
    ${DATA}=        Get Current Date
    [Return]        ${DATA}

Acrescentar dias à data atual
    [Arguments]     ${DIAS}
    ${DATA}=        Obter data atual
    ${DATA}=        Add Time To Date    ${DATA}         ${DIAS}
    ${DATA}=                            Convert Date    ${DATA}     result_format=%d/%m/%Y
    [Return]                            ${DATA}